import { Component, ChangeDetectionStrategy, signal } from '@angular/core'
import { FormBuilder, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { catchError, finalize, mergeMap, take, tap, throwError } from 'rxjs'

import { DialogService, UserService } from 'src/app/core'
import { AppRouterLinkPath } from 'src/app/shared/enums'

@Component({
  selector: 'app-sign-up-and-sign-in',
  templateUrl: './sign-up-and-sign-in.component.html',
  styleUrls: ['./sign-up-and-sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignUpAndSignInComponent {
  signUpMode = signal<boolean>(true)
  formGroup = this.fb.group({
    email: this.fb.control('', {
      validators: [Validators.required],
      nonNullable: true
    }),
    password: this.fb.control('', {
      validators: [Validators.required],
      nonNullable: true
    })
  })
  get emailControl(): FormControl {
    return this.formGroup.controls.email
  }
  get passwordControl(): FormControl {
    return this.formGroup.controls.password
  }
  showFormControlError = signal<boolean>(false)
  saving = signal<boolean>(false)

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private dialogService: DialogService
  ) {}

  signIn(): void {
    const { email, password } = this.formGroup.value

    if (this.formGroup.valid) {
      this.showFormControlError.set(false)
      this.saving.set(true)

      this.userService
        .signIn(email!, password!)
        .pipe(
          catchError((err) => {
            const code = err?.code

            if (code?.includes('auth/invalid-email')) {
              this.emailControl.setErrors({ invalidEmail: true })
              this.showFormControlError.set(true)
            } else if (code?.includes('auth/invalid-login-credentials')) {
              this.passwordControl.setErrors({ invalidLoginCredentials: true })
              this.showFormControlError.set(true)
            } else this.dialogService.openErrorModal()

            return throwError(() => err)
          }),
          finalize(() => this.saving.set(false)),
          mergeMap(() => this.userService.initUser()),
          tap(() => this.router.navigateByUrl(AppRouterLinkPath.ChatRooms)),
          take(1)
        )
        .subscribe()
    } else this.showFormControlError.set(true)
  }

  signUp(): void {
    const { email, password } = this.formGroup.value

    if (this.formGroup.valid) {
      this.showFormControlError.set(false)
      this.saving.set(true)

      this.userService
        .signUp(email!, password!)
        .pipe(
          catchError((err) => {
            const code = err?.code

            if (code?.includes('auth/invalid-email')) {
              this.emailControl.setErrors({ invalidEmail: true })
              this.showFormControlError.set(true)
            } else if (code.includes('auth/email-already-in-use')) {
              this.emailControl.setErrors({ emailAlreadyInUse: true })
              this.showFormControlError.set(true)
            } else if (code?.includes('auth/weak-password')) {
              this.passwordControl.setErrors({ invalidPassword: true })
              this.showFormControlError.set(true)
            } else this.dialogService.openErrorModal()

            return throwError(() => err)
          }),
          finalize(() => this.saving.set(false)),
          mergeMap(() => this.userService.initUser()),
          tap(() => this.router.navigateByUrl(AppRouterLinkPath.ChatRooms)),
          take(1)
        )
        .subscribe()
    } else this.showFormControlError.set(true)
  }
}

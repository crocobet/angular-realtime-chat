import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Auth, GoogleAuthProvider, EmailAuthProvider } from '@angular/fire/auth'

import { auth } from 'firebaseui'

@Component({
  selector: 'app-firebase-auth',
  templateUrl: './firebase-auth.component.html',
  styleUrls: ['./firebase-auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FirebaseAuthComponent implements OnInit {
  constructor(private auth: Auth) {}

  ngOnInit(): void {
    this.displayFirebaseuiAuth()
  }

  displayFirebaseuiAuth(): void {
    const ui = new auth.AuthUI(this.auth)

    ui.start('#firebaseui-auth-container', {
      signInSuccessUrl: '/chat-rooms',
      signInOptions: [
        GoogleAuthProvider.PROVIDER_ID,
        {
          provider: EmailAuthProvider.PROVIDER_ID,
          signInMethod: EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD
        }
      ]
    })
  }
}

import { NgModule } from '@angular/core'

// Angular Modules
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'

// Angular Material Modules
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'

// Components
import { SignUpAndSignInComponent } from './sign-up-and-sign-in/sign-up-and-sign-in.component'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  declarations: [SignUpAndSignInComponent],
  exports: [SignUpAndSignInComponent]
})
export class AuthModule {}

/**
 * Routing enums are defined specifically for consistency. In case the value(s)
 * change, this change will be reflected in every part of the application, and
 * if the property names are changed, property definition errors will be thrown,
 * so these kind of errors won't be silently retained in the application.
 *
 * `enum <module-name>RoutePath` enums define the route definitions of
 * `*-routing.module.ts` files. `enum <module-name>RouterLinkPath` enums define
 * values for `routerLink` directives, `Router.navigateByUrl` and
 * `Router.navigate` methods. These two aforementioned enum types are synced
 * (meaning it is manually synced, when you add value in one, you should add the
 * same value in to the other) with each other.
 */

export enum AppRoutePath {
  Landing = '',
  Profile = 'profile',
  ChatRooms = 'chat-rooms',
  FallbackRoute = '**'
}

export enum AppRouterLinkPath {
  Landing = `/${AppRoutePath.Landing}`,
  Profile = `/${AppRoutePath.Profile}`,
  ChatRooms = `/${AppRoutePath.ChatRooms}`
}

export enum ProfileRoutePath {
  Main = ''
}

export enum ProfileRouterLinkPath {
  Main = ProfileRoutePath.Main
}

export enum ChatRoomsRoutePath {
  Main = ''
}

export enum ChatRoomsRouterLinkPath {
  Main = ChatRoomsRoutePath.Main
}

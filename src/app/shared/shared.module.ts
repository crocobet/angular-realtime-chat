import { NgModule } from '@angular/core'

// Angular Material Modules
import { MatDialogModule } from '@angular/material/dialog'

// Components
import { ErrorDialogComponent } from './components/error-dialog/error-dialog.component'
import { TypingAnimationComponent } from './components/typing-animation/typing-animation.component'

@NgModule({
  imports: [MatDialogModule],
  declarations: [ErrorDialogComponent, TypingAnimationComponent],
  exports: [MatDialogModule, ErrorDialogComponent, TypingAnimationComponent]
})
export class SharedModule {}

import { User } from './user'

export interface ChatRoom {
  name: string
  userUids: ChatRoomUserUids
}

export interface ChatRoomUserUids {
  [uid: User['uid']]: boolean
}

export interface ChatRoomWindow {
  chatRoomName: ChatRoom['name']
  messages: ChatRoomMessages
  keyboardTypingMaxDates: {
    [userUid: User['uid']]: null | number
  }
}

export interface ChatRoomMessages {
  [id: string]: ChatRoomMessage
}

export interface ChatRoomMessage {
  message: string
  date: number
  senderUid: User['uid']
}

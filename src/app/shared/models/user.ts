import { ChatRoom } from './chat-rooms'

export interface User {
  uid: string
  photoURL?: null | string
  displayName?: null | string
  status?: null | string
  chatRoomNames?: {
    [chatRoomName: ChatRoom['name']]: boolean
  }
  online?: boolean
  unreadMessages?: UserUnreadMessages
}

export interface UserUnreadMessages {
  [chatRoomName: ChatRoom['name']]: number
}

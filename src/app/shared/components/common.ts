import { Type, Provider, forwardRef } from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

/**
 * @function COMMON_CONTROL_VALUE_ACCESSOR_FACTORY
 * @description Use this factory function to create the provider object for
 * control value accessor implementation.
 */
export function COMMON_CONTROL_VALUE_ACCESSOR_FACTORY(
  Component: Type<any>
): Provider {
  return {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => Component),
    multi: true
  }
}

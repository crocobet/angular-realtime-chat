import { Component, ChangeDetectionStrategy } from '@angular/core'

@Component({
  selector: 'app-typing-animation',
  templateUrl: './typing-animation.component.html',
  styleUrls: ['./typing-animation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypingAnimationComponent {}

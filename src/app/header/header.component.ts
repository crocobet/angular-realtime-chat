import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  signal
} from '@angular/core'
import { Subject, takeUntil } from 'rxjs'

import { User } from 'src/app/shared/models/user'
import { AppRouterLinkPath } from 'src/app/shared/enums'
import { UserService } from 'src/app/core'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {
  currentUser = signal<null | User>(null)
  unreadMessages = signal<null | number>(0)
  $destroyed = new Subject<void>()
  readonly AppRouterLinkPath = AppRouterLinkPath

  constructor(public userService: UserService) {}

  ngOnInit(): void {
    this.listenToCurrentUserChange()
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()
  }

  private listenToCurrentUserChange(): void {
    this.userService.$currentUser
      .pipe(takeUntil(this.$destroyed))
      .subscribe((currentUser) => {
        this.currentUser.set(currentUser)

        if (currentUser) {
          this.unreadMessages.set(
            Object.values(currentUser.unreadMessages || {}).filter(
              (unreadMessage) => unreadMessage
            ).length
          )
        }
      })
  }

  goToChatroom(event: Event): void {
    event.preventDefault()
    event.stopPropagation()
  }

  signOut(): void {
    this.userService.signOut()
  }
}

import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  Input,
  signal
} from '@angular/core'
import { FormBuilder } from '@angular/forms'
import { MatDialog } from '@angular/material/dialog'
import { Observable, Subject, concatMap, map, takeUntil } from 'rxjs'

import { ChatRoom } from 'src/app/shared/models/chat-rooms'
import { User } from 'src/app/shared/models/user'
import { CreateChatRoomDialogComponent } from '../create-chat-room-dialog/create-chat-room-dialog.component'
import { UserService, ChatRoomService } from 'src/app/core'

@Component({
  selector: 'app-chat-rooms-list',
  templateUrl: './chat-rooms-list.component.html',
  styleUrls: ['./chat-rooms-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatRoomsListComponent implements OnInit, OnDestroy {
  @Input({ required: true }) standaloneMode?: boolean
  searchQueryFormGroup = this.fb.group({
    searchQuery: this.fb.control('', { nonNullable: true })
  })
  selectedChatRoom = signal<null | ChatRoom>(null)
  joinChatButtonHovered = false
  chatRooms = signal<null | ChatRoom[]>([])
  currentUser = signal<null | User>(null)
  $destroyed = new Subject<void>()

  constructor(
    private fb: FormBuilder,
    private matDialog: MatDialog,
    private userService: UserService,
    private chatRoomService: ChatRoomService
  ) {}

  ngOnInit(): void {
    this.chatRoomService.$chatRooms
      .pipe(takeUntil(this.$destroyed))
      .subscribe((chatRooms) => this.chatRooms.set(chatRooms))

    this.userService.$currentUser
      .pipe(takeUntil(this.$destroyed))
      .subscribe((currentUser) => this.currentUser.set(currentUser))

    this.searchQueryFormGroup.controls.searchQuery.valueChanges
      .pipe(takeUntil(this.$destroyed))
      .subscribe((searchQuery) =>
        this.chatRoomService.$searchQuery.next(searchQuery)
      )
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()
  }

  createChatRoom(): void {
    this.matDialog.open(CreateChatRoomDialogComponent)
  }

  chatRoomTrackByFn(index: number, chatRoom: ChatRoom): string {
    return chatRoom.name!
  }

  userTrackByFn(index: number, user: User): string {
    return user.uid
  }

  getChatRoomUser(
    chatRoomUserUids: ChatRoom['userUids']
  ): Observable<undefined | User> {
    return this.userService.$currentUser.pipe(
      concatMap((currentUser) => {
        let uids = Object.keys(chatRoomUserUids || {})

        if (uids.length > 1)
          uids = uids.filter((uid) => uid !== currentUser!.uid)

        const firstUserUid = uids[0]

        return this.chatRoomService.$chatUsers.pipe(
          map((chatUsers) =>
            chatUsers?.find((chatUser) => chatUser.uid === firstUserUid)
          )
        )
      })
    )
  }

  selectChatRoom(chatRoom: ChatRoom): void {
    if (!this.joinChatButtonHovered && this.chatRoomJoined(chatRoom))
      this.chatRoomService.$selectedChatRoom.next(chatRoom)
  }

  chatRoomJoined(chatRoom: ChatRoom): boolean {
    if (this.currentUser()?.chatRoomNames)
      return this.currentUser()!.chatRoomNames![chatRoom.name]
    else return false
  }

  joinChatRoom(event: Event, chatRoom: ChatRoom): void {
    event.preventDefault()
    const currentUserChatRoomsNames = this.currentUser()?.chatRoomNames || {}

    currentUserChatRoomsNames[chatRoom.name] = true
    this.userService
      .patchUser({
        chatRoomNames: currentUserChatRoomsNames
      })
      .pipe(
        concatMap(() => this.chatRoomService.fetchChatRooms(chatRoom.name)),
        map((chatRooms) => chatRooms[0]),
        concatMap((chatRoom) => {
          chatRoom.userUids[this.currentUser()!.uid] = true
          return this.chatRoomService.updateChatRoom(chatRoom)
        })
      )
      .subscribe(() => {
        this.joinChatButtonHovered = false
      })
  }
}

import {
  Component,
  ChangeDetectionStrategy,
  AfterViewInit,
  OnDestroy,
  signal,
  ViewChild,
  ElementRef
} from '@angular/core'
import { FormBuilder, Validators } from '@angular/forms'
import { MatDialogRef } from '@angular/material/dialog'
import {
  Subject,
  catchError,
  concatMap,
  finalize,
  take,
  takeUntil,
  throwError,
  timer
} from 'rxjs'

import { ChatRoomService, UserService, DialogService } from 'src/app/core'

@Component({
  selector: 'app-create-chat-room-dialog',
  templateUrl: './create-chat-room-dialog.component.html',
  styleUrls: ['./create-chat-room-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateChatRoomDialogComponent implements AfterViewInit, OnDestroy {
  @ViewChild('chatRoomNameInput')
  chatRoomNameInput!: ElementRef<HTMLInputElement>
  formGroup = this.fb.group({
    chatRoomName: this.fb.control('', {
      nonNullable: true,
      validators: Validators.required
    })
  })
  saving = signal<boolean>(false)
  $destroyed = new Subject<void>()

  constructor(
    private fb: FormBuilder,
    private matDialogRef: MatDialogRef<CreateChatRoomDialogComponent>,
    private chatRoomService: ChatRoomService,
    private userService: UserService,
    private dialogService: DialogService
  ) {}

  ngAfterViewInit(): void {
    timer(300).subscribe(() => {
      this.chatRoomNameInput.nativeElement.focus()
    })
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()
  }

  createChatRoom(): void {
    const chatRoomName = this.formGroup.controls.chatRoomName.value

    this.saving.set(true)
    if (this.formGroup.valid)
      this.chatRoomService
        .createChatRoom(chatRoomName)
        .pipe(
          concatMap((chatRoom) =>
            this.chatRoomService.createChatRoomWindow(chatRoom.name)
          ),
          concatMap(() => this.userService.$currentUser),
          concatMap((currentUser) =>
            this.userService.patchUser({
              chatRoomNames: {
                ...(currentUser?.chatRoomNames || {}),
                [chatRoomName]: true
              }
            })
          ),
          take(1),
          finalize(() => {
            this.saving.set(false)
            this.matDialogRef.close()
          }),
          catchError((err) => {
            this.dialogService.openErrorModal()
            return throwError(() => err)
          }),
          takeUntil(this.$destroyed)
        )
        .subscribe()
  }
}

import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  signal,
  Inject
} from '@angular/core'
import { MAT_DIALOG_DATA } from '@angular/material/dialog'
import { Subject, takeUntil } from 'rxjs'

import { ChatRoomService } from 'src/app/core'
import { ChatRoomUserUids } from 'src/app/shared/models/chat-rooms'
import { User } from 'src/app/shared/models/user'

export interface MatDialogData {
  userUids: ChatRoomUserUids
}

@Component({
  selector: 'app-chat-room-details-dialog',
  templateUrl: './chat-room-details-dialog.component.html',
  styleUrls: ['./chat-room-details-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatRoomDetailsDialogComponent implements OnInit, OnDestroy {
  users = signal<User[]>([])
  $destroyed = new Subject<void>()

  constructor(
    @Inject(MAT_DIALOG_DATA) public matDialogData: MatDialogData,
    private chatRoomService: ChatRoomService
  ) {}

  ngOnInit(): void {
    this.chatRoomService.$chatUsers
      .pipe(takeUntil(this.$destroyed))
      .subscribe((chatUsers) => {
        this.users.set(
          chatUsers.filter((chatUser) =>
            Object.keys(this.matDialogData.userUids).includes(chatUser.uid)
          )
        )
      })
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()
  }
}

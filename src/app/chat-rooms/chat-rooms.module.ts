import { NgModule } from '@angular/core'

// Angular Modules
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'

// Angular Material Modules
import { MatInputModule } from '@angular/material/input'
import { MatDialogModule } from '@angular/material/dialog'
import { MatButtonModule } from '@angular/material/button'

// App Modules
import { ChatRoomsRoutingModule } from './chat-rooms-routing.module'
import { SharedModule } from 'src/app/shared/shared.module'

// Components
import { ChatRoomsComponent } from './chat-rooms/chat-rooms.component'
import { MainComponent } from './main/main.component'
import { ChatRoomsListComponent } from './chat-rooms-list/chat-rooms-list.component'
import { ChatRoomWindowComponent } from './chat-room-window/chat-room-window.component'
import { ChatRoomDetailsDialogComponent } from './chat-room-details-dialog/chat-room-details-dialog.component'
import { AvatarWithOnlineStatusComponent } from './avatar-with-online-status/avatar-with-online-status.component'
import { CreateChatRoomDialogComponent } from './create-chat-room-dialog/create-chat-room-dialog.component'

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,

    MatInputModule,
    MatDialogModule,
    MatButtonModule,

    ChatRoomsRoutingModule,
    SharedModule
  ],
  declarations: [
    ChatRoomsComponent,
    MainComponent,
    ChatRoomsListComponent,
    ChatRoomWindowComponent,
    ChatRoomDetailsDialogComponent,
    AvatarWithOnlineStatusComponent,
    CreateChatRoomDialogComponent
  ]
})
export class ChatRoomsModule {}

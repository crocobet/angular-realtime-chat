import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Input,
  signal
} from '@angular/core'
import { Router } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import {
  Observable,
  Subject,
  catchError,
  combineLatest,
  concatMap,
  filter,
  forkJoin,
  map,
  mergeMap,
  of,
  switchMap,
  take,
  takeUntil,
  tap,
  throttleTime,
  throwError,
  timer
} from 'rxjs'

import {
  ChatRoomMessage,
  ChatRoomWindow
} from 'src/app/shared/models/chat-rooms'
import { User } from 'src/app/shared/models/user'
import { ChatRoomService, UserService, DialogService } from 'src/app/core'
import {
  ChatRoomDetailsDialogComponent,
  MatDialogData as ChatRoomDetailsDialogMatDialogData
} from '../chat-room-details-dialog/chat-room-details-dialog.component'
import { ENTER } from 'src/app/shared/constants/keyboard-event-codes'
import { AppRouterLinkPath } from 'src/app/shared/enums'

export interface UserUidsMappingToUsers {
  [uid: User['uid']]: User
}

@Component({
  selector: 'app-chat-room-window',
  templateUrl: './chat-room-window.component.html',
  styleUrls: ['./chat-room-window.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatRoomWindowComponent implements OnInit, OnDestroy {
  @ViewChild('conversation') conversation?: ElementRef<HTMLDivElement>
  @ViewChild('messageTextarea')
  messageTextarea?: ElementRef<HTMLTextAreaElement>
  @Input({ required: true }) standaloneMode?: boolean
  chatRoomWindow = signal<null | ChatRoomWindow>(null)
  messages = signal<null | ChatRoomMessage[]>([])
  users = signal<User[]>([])
  userUidsMappingToUsers = signal<UserUidsMappingToUsers>({})
  showTypingAnimation = signal<boolean>(false)
  $triggerTypingEvents = new Subject<void>()
  $destroyed = new Subject<void>()

  constructor(
    private router: Router,
    private matDialog: MatDialog,
    private chatRoomService: ChatRoomService,
    public userService: UserService,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.addTypingIndicatorListener()

    this.chatRoomService.$chatRoomWindow
      .pipe(
        filter((chatRoomWindow) => !!chatRoomWindow),
        map((chatRoomWindow) => {
          this.chatRoomWindow.set(chatRoomWindow)
          this.messages.set(
            Object.values(chatRoomWindow?.messages || {}).reverse()
          )
          this.evaluateTypingAnimationState().pipe(take(1)).subscribe()

          return chatRoomWindow
        }),
        takeUntil(this.$destroyed)
      )
      .subscribe(() => {
        this.initUsers()
          .pipe(
            tap(() => this.mapUserUidsToUsers()),
            concatMap(() => this.userService.$currentUser),
            concatMap((currentUser) =>
              this.userService.updateUnreadMessages(
                this.chatRoomWindow()?.chatRoomName!,
                currentUser!.uid,
                0
              )
            )
          )
          .subscribe()
      })
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()
  }

  private initUsers(): Observable<void> {
    return combineLatest({
      chatUsers: this.chatRoomService.$chatUsers,
      selectedChatRoom: this.chatRoomService.$selectedChatRoom
    }).pipe(
      mergeMap(({ chatUsers, selectedChatRoom }) => {
        const users = chatUsers.filter((chatUser) =>
          Object.keys(selectedChatRoom?.userUids || {}).includes(chatUser.uid)
        )
        this.users.set(users)

        return of(undefined)
      }),
      map(() => undefined)
    )
  }

  private mapUserUidsToUsers(): void {
    this.userUidsMappingToUsers.set(
      this.users().reduce((acc: UserUidsMappingToUsers, user) => {
        acc[user.uid!] = user
        return acc
      }, {})
    )
  }

  goBackToChatRoomsList(): void {
    this.router.navigateByUrl(AppRouterLinkPath.ChatRooms)
    this.chatRoomService.$selectedChatRoom.next(null)
  }

  openChatRoomWindowInfoDialog(): void {
    this.chatRoomService.$selectedChatRoom
      .pipe(take(1))
      .subscribe((selectedChatRoom) => {
        this.matDialog.open(ChatRoomDetailsDialogComponent, {
          data: {
            userUids: selectedChatRoom!.userUids
          } as ChatRoomDetailsDialogMatDialogData
        })
      })
  }

  getUserFirstName(senderUid: User['uid']): string {
    return this.userUidsMappingToUsers()[senderUid]?.displayName ?? ''
  }

  messageIsOlderThanToday(date: number): boolean {
    return new Date().getDate() > new Date(date).getDate()
  }

  evaluateTypingAnimationState(): Observable<void> {
    let maxDates = Object.entries(
      this.chatRoomWindow()?.keyboardTypingMaxDates || {}
    )

    return this.userService.$currentUser.pipe(
      take(1),
      map((currentUser) => {
        maxDates = maxDates.filter(([userUid]) => userUid !== currentUser!.uid)

        for (const [, maxDate] of maxDates) {
          let duration = 0

          if (typeof maxDate === 'number')
            duration = maxDate - new Date().getTime()

          if (duration > 0) {
            this.showTypingAnimation.set(true)
            timer(duration).subscribe(() => this.showTypingAnimation.set(false))
            return
          }
        }

        this.showTypingAnimation.set(false)
      })
    )
  }

  handleTextareaChange(v: Event): void {
    this.$triggerTypingEvents.next()
  }

  addTypingIndicatorListener(): void {
    this.$triggerTypingEvents
      .pipe(
        throttleTime(2e3),
        mergeMap(() => this.userService.$currentUser),
        mergeMap((currentUser) =>
          this.chatRoomService.updateChatRoomWindow({
            ...this.chatRoomWindow()!,
            keyboardTypingMaxDates: {
              ...(this.chatRoomWindow()?.keyboardTypingMaxDates || {}),
              [currentUser!.uid]: new Date().getTime() + 60000 // 60000ms = 1 minute
            }
          })
        ),
        switchMap(() => timer(4e3)),
        mergeMap(() => this.stopTypingIndicator()),
        takeUntil(this.$destroyed)
      )
      .subscribe()
  }

  private stopTypingIndicator(): Observable<void> {
    return this.userService.$currentUser.pipe(
      mergeMap((currentUser) =>
        this.chatRoomService.updateChatRoomWindow({
          ...this.chatRoomWindow()!,
          keyboardTypingMaxDates: {
            ...(this.chatRoomWindow()?.keyboardTypingMaxDates || {}),
            [currentUser!.uid]: null
          }
        })
      ),
      take(1)
    )
  }

  sendMessage(event?: Event): void {
    combineLatest({
      currentUser: this.userService.$currentUser,
      selectedChatRoom: this.chatRoomService.$selectedChatRoom
    })
      .pipe(take(1))
      .subscribe(({ currentUser, selectedChatRoom }) => {
        const textarea = this.messageTextarea!.nativeElement

        if (!textarea.value) return

        const message: ChatRoomMessage = {
          message: textarea.value,
          date: new Date().getTime(),
          senderUid: currentUser!.uid
        }

        if (event instanceof KeyboardEvent && event.code === ENTER)
          event?.preventDefault()

        textarea.value = ''

        this.chatRoomService
          .sendMessage(this.chatRoomWindow()!.chatRoomName, message)
          .pipe(
            mergeMap(() => this.stopTypingIndicator()),
            concatMap(() => this.userService.$currentUser),
            concatMap((currentUser) =>
              forkJoin(
                Object.keys(selectedChatRoom?.userUids || {})
                  .filter((uid) => uid !== currentUser!.uid)
                  .map((uid) =>
                    this.userService.updateUnreadMessages(
                      selectedChatRoom!.name,
                      uid
                    )
                  )
              )
            ),
            catchError((err) => {
              this.dialogService.openErrorModal()
              return throwError(() => err)
            })
          )
          .subscribe()
      })
  }
}

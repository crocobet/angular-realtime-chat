import { Component, ChangeDetectionStrategy, Input } from '@angular/core'
import { User } from 'src/app/shared/models/user'

@Component({
  selector: 'app-avatar-with-online-status',
  templateUrl: './avatar-with-online-status.component.html',
  styleUrls: ['./avatar-with-online-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarWithOnlineStatusComponent {
  @Input({ required: true }) user?: null | User
  @Input() size: 'small' | 'large' = 'large'
}

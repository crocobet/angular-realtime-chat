import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'

// Angular Modules
import { RouterModule } from '@angular/router'

// Components
import { ChatRoomsComponent } from './chat-rooms/chat-rooms.component'
import { MainComponent } from './main/main.component'

// Mixed
import { ChatRoomsRoutePath } from 'src/app/shared/enums/routing'

const routes: Routes = [
  {
    path: ChatRoomsRoutePath.Main,
    component: ChatRoomsComponent,
    children: [
      {
        path: ChatRoomsRoutePath.Main,
        component: MainComponent,
        pathMatch: 'full'
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoomsRoutingModule {}

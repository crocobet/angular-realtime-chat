import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  signal,
  Inject,
  PLATFORM_ID
} from '@angular/core'
import { isPlatformBrowser } from '@angular/common'
import {
  Subject,
  catchError,
  combineLatest,
  concatMap,
  debounceTime,
  filter,
  fromEvent,
  iif,
  map,
  mergeMap,
  of,
  switchMap,
  take,
  takeUntil,
  tap,
  throwError
} from 'rxjs'

import { ChatRoom } from 'src/app/shared/models/chat-rooms'
import { User } from 'src/app/shared/models/user'
import { UserService, ChatRoomService, DialogService } from 'src/app/core'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit, OnDestroy {
  standaloneMode = signal<boolean>(false)
  selectedChatRoom = signal<null | ChatRoom>(null)
  $destroyed = new Subject<void>()

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private chatRoomService: ChatRoomService,
    private userService: UserService,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.listenToChatRoomsAndChatUsersChanges()
    this.listenToChatRoomWindowChanges()
    this.evaluateStandaloneMode()
    this.listenToWindowResize()
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()

    this.chatRoomService.$searchQuery.next('')
    this.chatRoomService.$chatRooms.next([])
    this.chatRoomService.$chatUsers.next([])
    this.chatRoomService.$selectedChatRoom.next(null)
    this.chatRoomService.$chatRoomWindow.next(null)
  }

  private listenToChatRoomsAndChatUsersChanges(): void {
    combineLatest({
      searchQuery: this.chatRoomService.$searchQuery.pipe(debounceTime(400)),
      currentUser: this.userService.$currentUser
    })
      .pipe(
        switchMap(({ searchQuery, currentUser }) => {
          const chatRoomNames = searchQuery
            ? searchQuery
            : Object.keys(currentUser?.chatRoomNames || {})

          return this.chatRoomService.listenToChatRoomsChanges(chatRoomNames)
        }),
        filter((chatRoom) => !!chatRoom),
        map((chatRoom) => {
          let chatRooms = this.chatRoomService.$chatRooms.value
          const foundChatRoomIndex = chatRooms.findIndex(
            ({ name }) => name === chatRoom.name
          )

          if (foundChatRoomIndex !== -1)
            chatRooms[foundChatRoomIndex] = chatRoom
          else chatRooms.push(chatRoom)

          chatRooms = chatRooms.filter((chatRoom) => chatRoom)
          this.chatRoomService.$chatRooms.next(chatRooms)
          this.updateSelectedChatRoom()

          return chatRooms
        }),
        switchMap((chatRooms) => {
          const chatUserUids: User['uid'][] = chatRooms
            .reduce((acc: User['uid'][], chatRoom) => {
              acc.push(...Object.keys(chatRoom.userUids || {}))
              return acc
            }, [])
            .filter((uid, i, arr) => arr.indexOf(uid) === i)

          return this.chatRoomService.listenToChatUsersChanges(chatUserUids)
        }),
        tap((chatUser) => {
          const chatUsers = this.chatRoomService.$chatUsers.value
          const foundChatUserIndex = chatUsers.findIndex(
            ({ uid }) => uid === chatUser.uid
          )

          if (foundChatUserIndex !== -1)
            chatUsers[foundChatUserIndex] = chatUser
          else chatUsers.push(chatUser)

          this.chatRoomService.$chatUsers.next(chatUsers)
        }),
        catchError((err) => {
          this.dialogService.openErrorModal()
          return throwError(() => err)
        }),
        takeUntil(this.$destroyed)
      )
      .subscribe()
  }

  private listenToChatRoomWindowChanges(): void {
    this.chatRoomService.$selectedChatRoom
      .pipe(
        tap((selectedChatRoom) => this.selectedChatRoom.set(selectedChatRoom)),
        switchMap((selectedChatRoom) =>
          iif(
            () => !!selectedChatRoom,
            of(selectedChatRoom).pipe(
              filter((selectedChatRoom) => !!selectedChatRoom?.name),
              switchMap((selectedChatRoom) =>
                this.chatRoomService.listenToChatRoomWindowChanges(
                  selectedChatRoom!.name
                )
              )
            ),
            of(null)
          )
        ),
        map((chatRoomWindow) => {
          this.chatRoomService.$chatRoomWindow.next(chatRoomWindow)
          return chatRoomWindow
        }),
        catchError((err) => {
          this.dialogService.openErrorModal()
          return throwError(() => err)
        }),
        takeUntil(this.$destroyed)
      )
      .subscribe()
  }

  private updateSelectedChatRoom(): void {
    if (this.selectedChatRoom()) {
      this.chatRoomService.$chatRooms.pipe(take(1)).subscribe((chatRooms) => {
        const chatRoom = chatRooms.find(
          (chatRoom) => chatRoom.name === this.selectedChatRoom()!.name
        )
        if (chatRoom) this.chatRoomService.$selectedChatRoom.next(chatRoom)
      })
    }
  }

  private evaluateStandaloneMode(): boolean {
    if (isPlatformBrowser(this.platformId))
      this.standaloneMode.set(window.innerWidth < 576)

    return false
  }

  private listenToWindowResize(): void {
    fromEvent(window, 'resize')
      .pipe(takeUntil(this.$destroyed))
      .subscribe(() => this.evaluateStandaloneMode())
  }
}

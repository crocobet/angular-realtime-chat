import { ModuleWithProviders, NgModule } from '@angular/core'

// Angular Modules
import { MatDialogModule } from '@angular/material/dialog'

// Services
import {
  ChatRoomService,
  DialogService,
  MixedUtilsService,
  UserService
} from '../core/services'

// Guards
import { AuthGuard, LandingGuard } from './guards'

@NgModule({
  imports: [MatDialogModule]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ChatRoomService,
        MixedUtilsService,
        DialogService,
        UserService,

        AuthGuard,
        LandingGuard
      ]
    }
  }
}

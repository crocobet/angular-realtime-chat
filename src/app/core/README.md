The `CoreModule` provides the necessary features for the app to function e.g.
services and guards, which are vital throughout the app. It differs from the
`SharedModule` because of the fact that the latter contains the features that
may not be necessary for all the modules.

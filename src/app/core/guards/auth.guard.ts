import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Observable, map } from 'rxjs'

import { UserService } from '../services'
import { AppRouterLinkPath } from 'src/app/shared/enums'

@Injectable()
export class AuthGuard {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.userService.$signedIn.pipe(
      map((signedIn) => {
        if (!signedIn) {
          this.router.navigateByUrl(AppRouterLinkPath.Landing)
          return false
        }

        return true
      })
    )
  }
}

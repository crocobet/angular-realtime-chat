import { Injectable } from '@angular/core'
import {
  Database,
  ref as databaseRef,
  set,
  get,
  onValue,
  push
} from '@angular/fire/database'
import {
  BehaviorSubject,
  Observable,
  ReplaySubject,
  concatMap,
  finalize,
  forkJoin,
  from,
  map,
  of,
  take
} from 'rxjs'

import {
  ChatRoom,
  ChatRoomMessage,
  ChatRoomWindow
} from 'src/app/shared/models/chat-rooms'
import { User } from 'src/app/shared/models/user'
import {
  REALTIME_DATABASE_CHAT_ROOMS_FOLDER,
  REALTIME_DATABASE_CHAT_ROOM_WINDOWS,
  REALTIME_DATABASE_USERS_FOLDER
} from 'src/app/shared'
import { UserService } from './user.service'

@Injectable()
export class ChatRoomService {
  readonly $searchQuery = new BehaviorSubject<string>('')
  readonly $chatRooms = new BehaviorSubject<ChatRoom[]>([])
  readonly $chatUsers = new BehaviorSubject<User[]>([])
  readonly $selectedChatRoom = new BehaviorSubject<null | ChatRoom>(null)
  readonly $chatRoomWindow = new BehaviorSubject<null | ChatRoomWindow>(null)

  constructor(private database: Database, private userService: UserService) {}

  createChatRoom(chatRoomName: string): Observable<ChatRoom> {
    return this.userService.$currentUser.pipe(
      concatMap((currentUser) => {
        const reference = databaseRef(
          this.database,
          `${REALTIME_DATABASE_CHAT_ROOMS_FOLDER}/${chatRoomName}`
        )

        const newChatRoom: ChatRoom = {
          name: chatRoomName,
          userUids: { [currentUser!.uid]: true }
        }

        return from(set(reference, newChatRoom)).pipe(map(() => newChatRoom))
      }),
      take(1)
    )
  }

  updateChatRoom(updatedChatRoom: ChatRoom): Observable<ChatRoom> {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_CHAT_ROOMS_FOLDER}/${updatedChatRoom['name']}`
    )

    return from(set(reference, updatedChatRoom)).pipe(
      map(() => updatedChatRoom)
    )
  }

  createChatRoomWindow(
    chatRoomName: ChatRoom['name']
  ): Observable<ChatRoomWindow> {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_CHAT_ROOM_WINDOWS}/${chatRoomName}`
    )
    const newChatRoomWindow: ChatRoomWindow = {
      chatRoomName,
      messages: {},
      keyboardTypingMaxDates: {}
    }

    return from(set(reference, newChatRoomWindow)).pipe(
      map(() => newChatRoomWindow)
    )
  }

  fetchChatRooms(
    chatRoomNames: ChatRoom['name'] | ChatRoom['name'][]
  ): Observable<ChatRoom[]> {
    return of(
      (Array.isArray(chatRoomNames) ? chatRoomNames : [chatRoomNames]).map(
        (chatRoomName) =>
          databaseRef(
            this.database,
            `${REALTIME_DATABASE_CHAT_ROOMS_FOLDER}/${chatRoomName}`
          )
      )
    ).pipe(
      concatMap((references) =>
        forkJoin(
          references.map((reference) =>
            from(get(reference).then((snapshot) => snapshot.val()))
          )
        )
      )
    )
  }

  listenToChatRoomsChanges(
    chatRoomNames: ChatRoom['name'] | ChatRoom['name'][]
  ): Observable<ChatRoom> {
    const subject = new ReplaySubject<ChatRoom>(1)
    const unsubscribes = (
      Array.isArray(chatRoomNames) ? chatRoomNames : [chatRoomNames]
    )
      .map((chatRoomName) =>
        databaseRef(
          this.database,
          `${REALTIME_DATABASE_CHAT_ROOMS_FOLDER}/${chatRoomName}`
        )
      )
      .map((reference) =>
        onValue(
          reference,
          (snapshot) => subject.next(snapshot.val()),
          (error) => subject.error(error)
        )
      )

    return subject
      .asObservable()
      .pipe(
        finalize(() => unsubscribes.forEach((unsubscribe) => unsubscribe()))
      )
  }

  listenToChatUsersChanges(
    userUids: User['uid'] | User['uid'][]
  ): Observable<User> {
    const subject = new ReplaySubject<User>(1)
    const unsubscribes = (Array.isArray(userUids) ? userUids : [userUids])
      .map((userUid) =>
        databaseRef(
          this.database,
          `${REALTIME_DATABASE_USERS_FOLDER}/${userUid}`
        )
      )
      .map((reference) =>
        onValue(
          reference,
          (snapshot) => subject.next(snapshot.val()),
          (error) => subject.error(error)
        )
      )

    return subject
      .asObservable()
      .pipe(
        finalize(() => unsubscribes.forEach((unsubscribe) => unsubscribe()))
      )
  }

  listenToChatRoomWindowChanges(
    chatRoomName: ChatRoom['name']
  ): Observable<ChatRoomWindow> {
    const subject = new ReplaySubject<ChatRoomWindow>(1)
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_CHAT_ROOM_WINDOWS}/${chatRoomName}`
    )
    const unsubscribe = onValue(
      reference,
      (snapshot) => subject.next(snapshot.val()),
      (error) => subject.error(error)
    )

    return subject.asObservable().pipe(finalize(() => unsubscribe()))
  }

  updateChatRoomWindow(chatRoomWindow: ChatRoomWindow): Observable<void> {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_CHAT_ROOM_WINDOWS}/${chatRoomWindow.chatRoomName}`
    )

    return from(set(reference, chatRoomWindow)).pipe(map(() => undefined))
  }

  sendMessage(
    chatRoomName: ChatRoom['name'],
    message: ChatRoomMessage
  ): Observable<void> {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_CHAT_ROOM_WINDOWS}/${chatRoomName}/messages`
    )

    return from(push(reference, message)).pipe(map(() => undefined))
  }
}

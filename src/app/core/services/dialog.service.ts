import { Injectable } from '@angular/core'
import { MatDialog, MatDialogRef } from '@angular/material/dialog'

// Components
import { ErrorDialogComponent } from 'src/app/shared/components/error-dialog/error-dialog.component'

@Injectable()
export class DialogService {
  constructor(private matDialog: MatDialog) {}

  openErrorModal(): MatDialogRef<ErrorDialogComponent, void> {
    return this.matDialog.open(ErrorDialogComponent, { maxWidth: '97vw' })
  }
}

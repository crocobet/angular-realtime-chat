import { Injectable } from '@angular/core'
import { Observable, fromEvent, take, map } from 'rxjs'

@Injectable()
export class MixedUtilsService {
  encodeFileInBase64(file: File): Observable<string> {
    const fileReader = new FileReader()
    const fileReaderResult = fromEvent(fileReader, 'load')
      .pipe(take(1))
      .pipe(
        map(() =>
          typeof fileReader.result === 'string' ? fileReader.result : ''
        )
      )

    fileReader.readAsDataURL(file)

    return fileReaderResult
  }
}

import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import {
  Auth,
  updateProfile,
  User as FirebaseAuthUser,
  createUserWithEmailAndPassword,
  UserCredential,
  signInWithEmailAndPassword
} from '@angular/fire/auth'
import {
  Database,
  ref as databaseRef,
  get,
  onDisconnect,
  onValue,
  set
} from '@angular/fire/database'
import {
  Storage,
  ref as storageRef,
  uploadBytes,
  getDownloadURL
} from '@angular/fire/storage'
import {
  BehaviorSubject,
  concatMap,
  finalize,
  from,
  iif,
  map,
  mergeMap,
  Observable,
  of,
  ReplaySubject,
  Subject,
  take,
  tap
} from 'rxjs'

import { User } from 'src/app/shared/models/user'
import { ChatRoom } from 'src/app/shared/models/chat-rooms'
import { AppRouterLinkPath } from 'src/app/shared/enums'
import {
  STORAGE_USERS_FOLDER,
  STORAGE_USERS_PROFILE_IMAGES_FOLDER,
  REALTIME_DATABASE_USERS_FOLDER
} from 'src/app/shared'

@Injectable()
export class UserService {
  readonly $currentUser = new ReplaySubject<null | User>(1)
  readonly $signedIn: Observable<boolean> = this.$currentUser.pipe(
    map((currentUser) => currentUser !== null)
  )

  constructor(
    private router: Router,
    private database: Database,
    private storage: Storage,
    private auth: Auth
  ) {
    this.initUser().subscribe()
  }

  initUser(): Observable<null | User> {
    let userInitiallyUpdated = false

    return this.getFirebaseAuthUser().pipe(
      mergeMap((firebaseAuthUser) =>
        iif(
          () => !!firebaseAuthUser,
          of(firebaseAuthUser).pipe(
            concatMap((firebaseAuthUser) =>
              this.listenToUserChanges(firebaseAuthUser!.uid).pipe(
                map((currentUser) => {
                  const updatedUser: User = {
                    photoURL: firebaseAuthUser?.photoURL,
                    displayName: firebaseAuthUser?.displayName,
                    ...(currentUser || {}),
                    uid: firebaseAuthUser!.uid,
                    online: true
                  }

                  return updatedUser
                })
              )
            ),
            concatMap((user) => {
              if (!userInitiallyUpdated) return this.updateUser(user)
              else return of(user)
            }),
            tap((user) => {
              userInitiallyUpdated = true
              this.syncCurrentUserOnlineStatus(user)
            })
          ),
          of(firebaseAuthUser)
        )
      ),
      tap((currentUser) => {
        if (currentUser) this.syncCurrentUserOnlineStatus(currentUser)
      }),
      map((user) => {
        this.$currentUser.next(user)
        return user
      })
    )
  }

  signUp(email: string, password: string): Observable<UserCredential> {
    return from(createUserWithEmailAndPassword(this.auth, email, password))
  }

  signIn(email: string, password: string): Observable<UserCredential> {
    return from(signInWithEmailAndPassword(this.auth, email, password))
  }

  private getFirebaseAuthUser(): Observable<null | FirebaseAuthUser> {
    const subject = new Subject<null | FirebaseAuthUser>()

    this.auth.onAuthStateChanged(
      (user) => subject.next(user),
      (error) => subject.error(error)
    )

    return subject.asObservable()
  }

  private syncCurrentUserOnlineStatus(currentUser: User): void {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_USERS_FOLDER}/${currentUser.uid}/online`
    )

    onDisconnect(reference).set(false)
  }

  signOut(): void {
    from(this.auth.signOut())
      .pipe(take(1))
      .subscribe(() => {
        this.$currentUser.next(null)
        this.router.navigateByUrl(AppRouterLinkPath.Landing)
      })
  }

  uploadCurrentUserProfileImage(file: Blob): Observable<string> {
    const extractFileExtension = (fileType: string): string => {
      const [type, extension] = fileType.split('/')

      if (extension) return extension
      else return type
    }

    return this.$currentUser.pipe(
      concatMap((currentUser) => {
        const fileStoragePath = `${STORAGE_USERS_FOLDER}/${STORAGE_USERS_PROFILE_IMAGES_FOLDER}/${
          currentUser!.uid
        }.${extractFileExtension(file.type)}`
        const reference = storageRef(this.storage, fileStoragePath)

        return from(uploadBytes(reference, file)).pipe(
          concatMap(() => from(getDownloadURL(reference))),
          take(1)
        )
      }),
      take(1)
    )
  }

  fetchUser(uid: User['uid']): Observable<User> {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_USERS_FOLDER}/${uid}`
    )

    return from(get(reference).then((snapshot) => snapshot.val()))
  }

  listenToUserChanges(uid: User['uid']): Observable<User> {
    const subject = new ReplaySubject<User>(1)
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_USERS_FOLDER}/${uid}`
    )
    const unsubscribe = onValue(
      reference,
      (snapshot) => subject.next(snapshot.val()),
      (error) => subject.error(error)
    )

    return subject.asObservable().pipe(finalize(() => unsubscribe()))
  }

  updateCurrentUserFirebaseAuthProfile(
    user: Partial<FirebaseAuthUser>
  ): Observable<FirebaseAuthUser> {
    return this.getFirebaseAuthUser().pipe(
      concatMap((firebaseAuthUser) => {
        const updatedUser: any = {
          ...(firebaseAuthUser?.toJSON() as FirebaseAuthUser)
        }

        if (updatedUser.photoURL) updatedUser.photoURL = user.photoURL
        if (updatedUser.displayName) updatedUser.displayName = user.displayName

        return from(updateProfile(firebaseAuthUser!, updatedUser)).pipe(
          map(() => updatedUser)
        )
      }),
      take(1)
    )
  }

  updateUser(user: User): Observable<User> {
    const reference = databaseRef(
      this.database,
      `${REALTIME_DATABASE_USERS_FOLDER}/${user.uid}`
    )

    return from(set(reference, user)).pipe(map(() => user))
  }

  patchUser(user: Partial<User>): Observable<User> {
    return this.$currentUser.pipe(
      take(1),
      concatMap((currentUser) => {
        const reference = databaseRef(
          this.database,
          `${REALTIME_DATABASE_USERS_FOLDER}/${currentUser!.uid}`
        )
        const updatedUser: User = { ...currentUser!, ...user }

        return from(set(reference, updatedUser)).pipe(map(() => updatedUser))
      })
    )
  }

  updateUnreadMessages(
    chatRoomName: ChatRoom['name'],
    userUid: User['uid'],
    unreadCount?: number
  ): Observable<void> {
    return this.fetchUser(userUid).pipe(
      concatMap((user) => {
        const reference = databaseRef(
          this.database,
          `${REALTIME_DATABASE_USERS_FOLDER}/${user.uid}/unreadMessages/${chatRoomName}`
        )
        let _unreadCount: number

        if (!user!.unreadMessages) user!.unreadMessages = {}
        if (typeof unreadCount === 'number') _unreadCount = unreadCount
        else {
          _unreadCount = user!.unreadMessages[chatRoomName] || 0
          ++_unreadCount
        }

        return from(set(reference, _unreadCount))
      }),
      map(() => undefined),
      take(1)
    )
  }
}

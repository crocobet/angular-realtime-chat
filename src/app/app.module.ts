import { NgModule } from '@angular/core'

// Angular Modules
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ReactiveFormsModule } from '@angular/forms'

// Angular Material Modules
import { MatSelectModule } from '@angular/material/select'

// App Modules
import { AppRoutingModule } from './app-routing.module'
import { CoreModule } from 'src/app/core/core.module'

// Components
import { AppComponent } from './app.component'
import { HeaderComponent } from './header/header.component'

// @angular/fire
import { initializeApp, provideFirebaseApp } from '@angular/fire/app'
import { provideAuth, getAuth } from '@angular/fire/auth'
import { provideDatabase, getDatabase } from '@angular/fire/database'
import { provideFirestore, getFirestore } from '@angular/fire/firestore'
import { provideStorage, getStorage } from '@angular/fire/storage'

import { environment } from '../environments/environment'

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    MatSelectModule,

    AppRoutingModule,
    CoreModule.forRoot(),

    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth()),
    provideDatabase(() => getDatabase()),
    provideFirestore(() => getFirestore()),
    provideStorage(() => getStorage())
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

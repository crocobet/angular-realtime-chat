import { NgModule } from '@angular/core'

// Angular Modules
import { ReactiveFormsModule } from '@angular/forms'

// Angular Material Modules
import { MatButtonModule } from '@angular/material/button'
import { MatInputModule } from '@angular/material/input'
import { MatIconModule } from '@angular/material/icon'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'

// App Modules
import { ProfileRoutingModule } from './profile-routing.module'

// Components
import { ProfileComponent } from './profile/profile.component'
import { MainComponent } from './main/main.component'
import { ImageUploadAndPreviewComponent } from './image-upload-and-preview/image-upload-and-preview.component'

@NgModule({
  imports: [
    ReactiveFormsModule,

    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatProgressSpinnerModule,

    ProfileRoutingModule
  ],
  declarations: [
    ProfileComponent,
    MainComponent,
    ImageUploadAndPreviewComponent
  ]
})
export class ProfileModule {}

import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'

// Angular Modules
import { RouterModule } from '@angular/router'

// Components
import { ProfileComponent } from './profile/profile.component'
import { MainComponent } from './main/main.component'

// Mixed
import { ProfileRoutePath } from 'src/app/shared/enums/routing'

const routes: Routes = [
  {
    path: ProfileRoutePath.Main,
    component: ProfileComponent,
    children: [
      {
        path: ProfileRoutePath.Main,
        component: MainComponent,
        pathMatch: 'full'
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}

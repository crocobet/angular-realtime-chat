import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  signal
} from '@angular/core'
import { FormBuilder, Validators } from '@angular/forms'
import {
  Subject,
  catchError,
  concatMap,
  finalize,
  firstValueFrom,
  forkJoin,
  take,
  throwError
} from 'rxjs'

import { UserService, DialogService } from 'src/app/core'
import { User } from 'src/app/shared/models/user'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit, OnDestroy {
  formGroup = this.fb.group({
    photoData: this.fb.control<[string?, Blob?]>([]),
    displayName: this.fb.control('', Validators.required),
    status: this.fb.control('')
  })
  saving = signal<boolean>(false)
  $destroyed = new Subject<void>()

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.listenToCurrentUserChange()
  }

  ngOnDestroy(): void {
    this.$destroyed.next()
    this.$destroyed.complete()
  }

  listenToCurrentUserChange(): void {
    this.userService.$currentUser.pipe(take(1)).subscribe((currentUser) =>
      this.formGroup.patchValue({
        photoData: currentUser?.photoURL ? [currentUser?.photoURL] : [],
        displayName: currentUser?.displayName ?? '',
        status: currentUser?.status ?? ''
      })
    )
  }

  setNewBlobImage(newBlobImage: [string, Blob]): void {
    this.formGroup.controls.photoData.setValue(newBlobImage)
  }

  async save(): Promise<void> {
    const { photoData, displayName, status } = this.formGroup.value
    const newPhoto = photoData?.length && photoData[1]

    if (this.formGroup.valid) {
      const updatedUser: {
        uid: User['uid']
        displayName: string
        photoURL?: string
      } = {
        uid: '',
        displayName: displayName ?? ''
      }

      if (newPhoto)
        updatedUser.photoURL = await firstValueFrom(
          this.userService.uploadCurrentUserProfileImage(newPhoto)
        )
      this.saving.set(true)

      this.userService.$currentUser
        .pipe(
          concatMap((currentUser) => {
            updatedUser.uid = currentUser!.uid

            return forkJoin([
              this.userService.updateCurrentUserFirebaseAuthProfile(
                updatedUser
              ),
              this.userService.patchUser({
                ...updatedUser,
                status
              })
            ])
          }),
          catchError((err) => {
            this.dialogService.openErrorModal()
            return throwError(() => err)
          }),
          finalize(() => this.saving.set(false)),
          take(1)
        )
        .subscribe()
    }
  }
}

import { Component, ChangeDetectionStrategy, signal } from '@angular/core'
import { ControlValueAccessor } from '@angular/forms'
import { take } from 'rxjs'

import { MixedUtilsService } from 'src/app/core'
import { COMMON_CONTROL_VALUE_ACCESSOR_FACTORY } from 'src/app/shared/components/common'

@Component({
  selector: 'app-image-upload-and-preview',
  templateUrl: './image-upload-and-preview.component.html',
  styleUrls: ['./image-upload-and-preview.component.scss'],
  providers: [
    COMMON_CONTROL_VALUE_ACCESSOR_FACTORY(ImageUploadAndPreviewComponent)
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageUploadAndPreviewComponent implements ControlValueAccessor {
  photoData = signal<[string?, Blob?]>([])
  uploadButtonDisabled = signal<boolean>(false)
  onChange?: (photoData: [string, Blob]) => void

  constructor(private mixedUtilsService: MixedUtilsService) {}

  handleUploadedImage(event: Event): void {
    const fileElement: HTMLInputElement = event.target as HTMLInputElement
    let file: null | File = null

    if (fileElement?.files) file = fileElement.files[0]

    if (file)
      this.mixedUtilsService
        .encodeFileInBase64(file)
        .pipe(take(1))
        .subscribe((base64Image) => {
          const updatedPhotoData: [string, Blob] = [base64Image, file!]

          this.onChange!(updatedPhotoData)
          this.photoData.set(updatedPhotoData)
        })
  }

  writeValue(imagePreviewSrc: [string, Blob]): void {
    this.photoData.set(imagePreviewSrc)
  }

  registerOnChange(fn: (value: [string, Blob]) => void): void {
    this.onChange = fn
  }

  registerOnTouched(): void {}

  setDisabledState(uploadButtonDisabled: boolean): void {
    this.uploadButtonDisabled.set(uploadButtonDisabled)
  }
}

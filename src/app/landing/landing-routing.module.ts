import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

// Components
import { LandingComponent } from './landing/landing.component'
import { MainComponent } from './main/main.component'

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [{ path: '', component: MainComponent, pathMatch: 'full' }]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule {}

import { NgModule } from '@angular/core'

// App Modules
import { LandingRoutingModule } from './landing-routing.module'
import { AuthModule } from 'src/app/auth/auth.module'

// Components
import { LandingComponent } from './landing/landing.component'
import { MainComponent } from './main/main.component'

@NgModule({
  imports: [LandingRoutingModule, AuthModule],
  declarations: [LandingComponent, MainComponent]
})
export class LandingModule {}

import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'

// Angular Modules
import { RouterModule } from '@angular/router'

// Guards
import { AuthGuard, LandingGuard } from 'src/app/core'

// Mixed
import { AppRoutePath } from 'src/app/shared/enums'

const routes: Routes = [
  {
    path: AppRoutePath.Landing,
    loadChildren: () =>
      import('src/app/landing/landing.module').then((m) => m.LandingModule),
    canActivate: [LandingGuard]
  },
  {
    path: AppRoutePath.Profile,
    loadChildren: () =>
      import('src/app/profile/profile.module').then((m) => m.ProfileModule),
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutePath.ChatRooms,
    loadChildren: () =>
      import('src/app/chat-rooms/chat-rooms.module').then(
        (m) => m.ChatRoomsModule
      ),
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutePath.FallbackRoute,
    redirectTo: AppRoutePath.Landing,
    pathMatch: 'full'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

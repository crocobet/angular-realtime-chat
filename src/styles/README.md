This folder contains all of the global styles of the application, which are then imported either in the `src/styles.scss` or `@use`d in the component style
sheets.

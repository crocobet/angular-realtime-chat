# Angular Realtime Chat

## Prerequisites

To install and start the app, install the following apps:

- NPM
- Node.js

## Installing the app dependencies

To install the app dependencies, run the following command:

```console
npm i
```

## Starting the app

To start the app, run the following command:

```console
ng s -o
```

P.S. During the build phase the following error is present:
![Build phase error](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/mixed/build-error.png?raw=true)
This error was present from the beginning, I don't have to do anything with
this. You can **fix** it by just going to that type definition file in the
`node_modules` (and the relative path is `node_modules/rxfire/firestore/lite/interfaces.d.ts`) and on line 10, just remove this part of the type definitions
`, any, DocumentData`, save and the build should succeed.

## Tips for using the app

- Because of the fact that Firebase Realtime Database doesn't support fuzzy
  search, you should type the exact case-sensitive string in the search input,
  to find the chat room, otherwise, you won't find it. For example, if you are searching a chat room with the name "English Dictionary", you won't be able to
  find it if you type any of these keywords: "English dictionary", "English", "dictionary", etc.

## App images

- Sign up:

![Sign up](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/1.png?raw=true)

- User's menu opened:

![User's menu opened](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/2.png?raw=true)

- Profile update:

![Profile update](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/3.png?raw=true)

- Create chat room:

![Create chat room](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/4.png?raw=true)

- Chat window:

![Chat window](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/5.png?raw=true)

- Search the chat room and join:

![Search the chat room and join](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/6.png?raw=true)

- Typing in chat room:

![Typing in chat room](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/7.png?raw=true)

- Ant-Man sending a message:

![Ant-Man sending a message](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/8.png?raw=true)

- Deadpool sending a reply:

![Deadpool sending a reply](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/9.png?raw=true)

- Black Widow sends a reply:

![Black Widow sends a reply](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/10.png?raw=true)

- Another chat room is selected and the unread notification is present:

![Another chat room is selected and the unread notification is present](https://github.com/datomarjanidze/angular-realtime-chat/blob/main/src/assets/images/app-images/11.png?raw=true)
